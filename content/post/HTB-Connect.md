+++
author = "Daf3r"
title = "🐱‍💻 Como conectarte con OpenVPN a HackTheBox"
date = "2023-06-22"
description = "Un articulo mostrando como conectarte con la VPN a HackTheBox"
draft = false
tags = [
    "linux",
    "ciberseguridad",
    "hacking",
    "hacking ético",
]
categories = [
    "Hacking",
    "HackTheBox",
]
series = ["HackTheBox"]
+++

![MonitorsTwo](/images/HTB/htb.png)

Para conectarte con HackTheBox necesitaremos crear una cuenta e iniciar sesion en su website

https://app.hackthebox.com
## Cuenta en HackTheBox

Ya cuando tenemos la cuenta e iniciemos sesion primeramente nos iremos al apartado de connection

![MonitorsTwo](/images/HTB/connect.png) 

Seleccionaremos Machines para poder conectarnos a HTB labs para poder probar y hackear alguna de todas las maquinas disponibles

![MonitorsTwo](/images/HTB/machinesOVPN.png)

Luego seleccionaremos OpenVPN para conectarnos

![MonitorsTwo](/images/HTB/selectOVPN.png)

Ya seleccionado damos click en download y lo guardamos en cualquier lugar de nuestra maquina procedemos a instalar la VPN que nos permitira conectarnos a HTB -> "OpenVPN"

![MonitorsTwo](/images/HTB/Download.png)

## Conectandonos a HTB con OVPN

Ejecutamos este comando en consola:
```
sudo apt-get install openvpn
```
O si estas en arch

```
yay -S openvpn
```
Ya instalado nos conectamos a HTB con OpenVPN:

![MonitorsTwo](/images/HTB/connectopenvpn.png)

Aca dejo el comando a ejecutar para conectarnos

```bash
sudo openvpn TuArchivo.ovpn
```
Y finalmente estamos conectados con HTB empieza a hacker :P

![MonitorsTwo](/images/HTB/finish.png)
