+++
author = "Daf3r"
title = "📦 Stocker HTB - Easy Machine"
date = "2023-06-21"
description = "Un articulo mostrando como vulnerar la maquina Stocker en HackTheBox"
draft = false
tags = [
    "linux",
    "ciberseguridad",
    "hacking",
    "hacking ético",
]
categories = [
    "Hacking",
    "HackTheBox",
]
series = ["HackTheBox"]
+++

![MonitorsTwo](/images/Stocker/stocker.png)
## IP: 10.10.11.196
Empezamos haciendole ping a la maquina para ver si tenemos conexion:

![MonitorsTwo](/images/Stocker/ping.png)

Ejecutamos whichSystem.py aunque por ver el ttl de la maquina podriamos decir que es una maquina Linux:

![MonitorsTwo](/images/Stocker/whichSystem.png)

## Fase de reconocimiento

Aplicamos nmap para ver que puertos tiene la maquina abiertos

![MonitorsTwo](/images/Stocker/nmap.png)

Y ejecutamos el nmap para que que version y servicio corre para esos puertos

![MonitorsTwo](/images/Stocker/nmap2.png)

Para avanzar trabajo vemos con whatweb que corre detras de ese puerto 80 que este puerto se ocupa comunmente para servicios web:

![MonitorsTwo](/images/Stocker/whatweb.png)

Tenemos que colocar "stocker.htb" en hosts para poder comunicarnos con la pagina web

![MonitorsTwo](/images/Stocker/hosts.png)

Fuzzeamos con gobuster para ver que subdominios tiene la maquina ya que solo conocemos "stocker.htb" como direccion web local

![MonitorsTwo](/images/Stocker/gobuster.png)
## Empezamos a vulnerar la maquina
Encontramos que tiene un subdominio "dev.stocker.htb" Vamos a ver que tiene y ver que se puede vulnerar

![MonitorsTwo](/images/Stocker/devStocker.png)

Tenemos un login ponemos unas credenciales y tratamos de ver si podemos vulnerando interceptando la solicitud con burpsuite

![MonitorsTwo](/images/Stocker/burp.png)

Observamos un NoSQLI detras como base de datos tratamos de hacer una inyeccion para poder ingresar a la pagina dev

![MonitorsTwo](/images/Stocker/burp2.png)

Hemos podido ingresar

![MonitorsTwo](/images/Stocker/devpage.png)

Curoseando un poco hemos observado que para cada vez que se tramita un pedido nos trae un pdf inyectaremos XSS para que nos genere una imagen de prueba veremos que hay en "/etc/passwd"

```html
<iframe width=900 height=900 src=file:///etc/passwd></iframe>
```
![MonitorsTwo](/images/Stocker/burp3.png)

Enviamos la solicitud y esto nos mostro en el pdf

![MonitorsTwo](/images/Stocker/usersystem.png)

Entonces como es vulnerable podemos ver que hay en "/var/www/dev/index.js" porque este es el directorio base del sitio web

![MonitorsTwo](/images/Stocker/burp4.png)

![MonitorsTwo](/images/Stocker/pdfXSSCredentialsSSH.png)

Nos muestra un password intentemos acceder mediante el ssh que esta activo con el usuario "angoose" que estaba en "/etc/passwd"
```
IHeardPassphrasesArePrettySecure
```
![MonitorsTwo](/images/Stocker/userflag.png)

Ya que nos permitio ingresar leemos de una sola vez la user flag y buscamos una manera que nos permita escalar privilegio

![MonitorsTwo](/images/Stocker/ReverseShellRootJS.png )

Tenemos una via para escalar que el vulnerable en "/usr/local/script/*.js" hagamos un archivo en esa ruta que nos otorge una bash como root cuando ejecutemos el archivo

```html
const fs = require("child_process").spawn("/usr/bin/bash", {stdio: [0, 1, 2]})
```
![MonitorsTwo](/images/Stocker/rootflag.png )

Colocamos en el archivo que creamos el codigo anteriormente mencionado lo ejecutamos ya obtenemos una shell como administrador y hemos vulnerado la maquina

## PWND la maquina Stocker

![MonitorsTwo](/images/Stocker/pwnd.png )

https://www.hackthebox.com/achievement/machine/1497957/523
