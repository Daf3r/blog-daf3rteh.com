+++
author = "Daf3r"
title = "🐧 Me Linux Way"
date = "2023-06-13"
description = "Una articulo de prque deberiamos de escoger linux como sistema operativo"
draft = false
tags = [
    "linux",
    "privacidad",
]
categories = [
    "Linux",
]
series = ["Me"]
+++
![Linux Way](/images/LinuxWay/linux.png)<br>

Cuando tomé la decisión de escoger Linux como mi sistema base, fue un gran paso hacia un nuevo mundo por explorar. Ver qué podíamos descubrir fue un cambio drástico. He pasado de tener Windows a ser un usuario de Linux al 100% en un año y medio.

Empecé como la mayoría podría haber empezado, con Ubuntu, mi primera distribución cómoda y simple que permite a un usuario novato adentrarse en Linux sin necesidad de ejecutar comandos en la terminal. Sin ninguna personalización, estuve usando Ubuntu durante unos 4-5 meses para adaptarme al nuevo sistema operativo. Durante ese tiempo, descubrí que se podía instalar cualquier cosa sin ninguna restricción, se podía personalizar y había muchos comandos por aprender. Aprendí a compilar proyectos de GitHub para utilizar diferentes herramientas en ese momento, a configurar el firewall y muchas otras cosas básicas necesarias para ser un usuario común y corriente de Linux.

Pero llegó un momento en el que empecé a adentrarme en el mundo de la privacidad y me picó la curiosidad. ¿Se puede mejorar más el sistema? ¿Se puede encriptar el disco? ¿Qué más se puede hacer? Fue entonces cuando empecé a estudiar otras distribuciones, probándolas primero en VirtualBox antes de usarlas durante un tiempo. Así fue como probé dos distribuciones antes de llegar a mi sistema operativo base actual, que son PopOS y Fedora. Ambas me gustaron y las recomiendo. Son muy fáciles e intuitivas de usar, lo que ayuda a los usuarios de nivel básico-intermedio. Además, tienen la ventaja de poder encriptar el disco duro. Estuve utilizando estas distribuciones durante aproximadamente 6 meses, 3 meses cada una.

Luego descubrí el mundo de la ciberseguridad, otro amplio agujero de conejo, donde conocí a un chico llamado s4vitar, quien enseña a la gente sobre el hacking ético y algunas estrategias para vulnerar o hackear máquinas (el sueño de todo niño). He estado aprendiendo con él durante estos meses y he elevado aún más mi nivel de conocimiento sobre Linux en general. Tomé su curso de "Introducción a Linux", que abarca todo, desde lo más básico hasta programar en bash y realizar diferentes tareas. Durante el curso, se mostraba el trabajo en Parrot OS, que utilicé solo durante unas 2 semanas, hasta que descubrí Arch Linux.

Un amigo me dijo "buena suerte con esa distribución", lo que aumentó mi curiosidad. Seguí algunos tutoriales en VirtualBox sobre la instalación de Arch Linux, personalización de paquetes, y así pasé una tarde frente a mi portátil. Luego, comencé con la instalación de Arch Linux y unas horas después tenía todo configurado y ajustado a mis necesidades, listo para aprender sobre el hacking ético con s4vitar.

Y así es como me he quedado con Linux como mi sistema base, "Arch Linux".

![Linux Way](/images/LinuxWay/archlinux.png)

Les recomiendo dar ese salto, aprenderán muchas cosas y si te interesa la privacidad y seguridad de tu dispositivo, Linux es para ti.
