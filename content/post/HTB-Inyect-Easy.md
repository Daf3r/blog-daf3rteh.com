+++
author = "Daf3r"
title = "💉 Inject HTB - Easy Machine"
date = "2023-06-20"
description = "Un articulo mostrando como vulnerar la maquina Inject en HackTheBox"
draft = false
tags = [
    "linux",
    "ciberseguridad",
    "hacking",
    "hacking ético",
]
categories = [
    "Hacking",
    "HackTheBox",
]
series = ["HackTheBox"]
+++

![Inyect HTB](/images/Inject/inyect.png)
## IP: 10.10.11.204
Como siempre empezamos haciendo un ping a la maquina para ver si esta nos responde

![Inyect HTB](/images/Inject/ping.png)

Verificamos que sistema es aunque por simple vista del ttl se presume que es una linux:

![Inyect HTB](/images/Inject/whichSystem.png)

## Fase de reconocimiento
---
Ahora empezamos con la fase de reconocimiento empleando la herramienta nmap:

![Inyect HTB](/images/Inject/nmap.png)

Verificamos que version y servicio corren para cada uno de los puertos descubiertos:

![Inyect HTB](/images/Inject/sCV.png)

Empezamos a husmear un poco que hay en la web ya que vemos que el puerto 8080 esta aperturado y normalmente este puerto con el puerto 80 se ocupa para servidores web

![Inyect HTB](/images/Inject/web.png)

De momento no hay nada interesante para vulnerar exploramos todo pero no nos dice mucho

![Inyect HTB](/images/Inject/upload.png)

En el apartado de upload nos permite subir solamente imagenes no es vulnerable para podes filtrar un archivo.php por ahi pero al subir la imagen la podemos visualizar sera vulnerable ese campo?

![Inyect HTB](/images/Inject/upload2.png)

Probaremos si es vulnerable a file inclusion porque la web nos carga esto: “http://10.10.11.204:8080/show_image?img=347490005_923230325418001_6434907532651573016_n.jpg” y hay una imagen

![Inyect HTB](/images/Inject/image.png)

Podemos interceptar esto con burpsuite ya que a veces luego de "?image" suele ser un campo vulnerable para algunos frameworks y tenemos este datasheet para poder experimentar: https://book.hacktricks.xyz/pentesting-web/file-inclusion


Interceptamos la solicitud y lo mandamos al Repeater
---
```
GET /show_image?img=CampoVulnerable HTTP/1.1

Host: 10.10.11.204:8080

User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:114.0) Gecko/20100101 Firefox/114.0

Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8

Accept-Language: en-US,en;q=0.5

Accept-Encoding: gzip, deflate

DNT: 1

Connection: close

Referer: http://10.10.11.204:8080/upload

Upgrade-Insecure-Requests: 1

```
---

Aca modificamos la solicitud y verificamos que si es vulnerable a local file inclusion (LFI) Ejecutando y explorando con LFI encontramos esto sobre como esta construida la pagina web:

![Burpsuite](/images/Inject/burp2.png)


<div style="text-align: right"> your-text-here </div>![Burpsuite](/images/burp5.png)

Descubrimos que la pagina web esta construida con el spring framework y segun la version 3.2.2 tenemos una vulnerabilidad que nos permitira ingresar a la maquina para poder seguir buscando metodos para escalar hasta el privilegio ROOT

![Inyect HTB](/images/Inject/burp7.png)

Pero antes de continuar en "/home/frank/.m2/settings.xml" tenemos este usuario que luego nos servira:

![Inyect HTB](/images/Inject/burp8.png)

## Empezamos a vulnerar la pagina
Con eso no es posible acceder a ssh pero nos servira cuando estemos dentro del sistema asi que vamos con nuestro amigo gugel a buscar algun exploit para SpringFramework en la version 3.2.2

![Inyect HTB](/images/Inject/vuln.png)

Ocupamos el primero y lo ejecutamos con metasploit por aca adjunto su uso basico:

![Inyect HTB](/images/Inject/metasploit.png)

![Inyect HTB](/images/Inject/exploit.png)

y claro debemos tener abierto el puerto 4444 para que se nos pueda otorgar la reverse shell de aca ya podemos movernos hacia los directorios home pero como no es netamente una bash la que se nos ha otorgado no podremos ver o tener algunos permisos para ejecutar comandos ya obteniendo una shell podemos ver si la password que obtuvimos anteriormente sirve para entrar como el usuario phil:

![Inyect HTB](/images/Inject/phil.png)

esa era el password correcto asi que estamos dentro como el usuario phil:

![Inyect HTB](/images/Inject/FlagUser.png)

Descubrimos algo en automation que ejecuta playbook_x.yml como root existe esa vulnerabilidad asi que nos aprovechamos de ella

https://exploit-notes.hdks.org/exploit/linux/privilege-escalation/ansible-playbook-privilege-escalation/

![Inyect HTB](/images/Inject/automation.png)

![Inyect HTB](/images/Inject/automation2.png)

Para poder explotar esa vulnerabilidad debemos de hacer un "playbook_2.yml de manera local para luego enviarlo a la maquina victima en un servidor http con python:

![Inyect HTB](/images/Inject/playbook.png)

![Inyect HTB](/images/Inject/reverseshell.png)

![Inyect HTB](/images/Inject/bash.png)

Si nos fijamos se ha cambiado a 2 y procedemos a ponernos a poner “/bin/bash -p” en este caso y nos otorgara una bash como root y leemos la flag root.txt y la maquina ha sido vulnerada 

![Inyect HTB](/images/Inject/rootflag.png)
## PWND la maquina Inject
:star:
Daf3rTeh Pwnd la maquina Inject

![Inyect HTB](/images/Inject/inject.png)

https://www.hackthebox.com/achievement/machine/1497957/533



